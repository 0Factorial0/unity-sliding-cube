using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class player : MonoBehaviour
{
    public float speed;

    public Text SpeedText;

    void Start()
    {
        speed = 5f;
    }

    private void FixedUpdate()
    {
        //increase base speed
        speed += 0.1f;
        //write ui
        SpeedText.text = "Speed: " + Convert.ToInt32(speed).ToString();
    }

    void Update()
    {
        //move forward
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

        //move by keyboard
        //right
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * Time.deltaTime * speed / 2);
        }
        //left
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * Time.deltaTime * speed / 2);
        }
        //forward
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * Time.deltaTime * speed / 2);
        }
        //backward
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back * Time.deltaTime * speed / 2);
        }
    }
}
