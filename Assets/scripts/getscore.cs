using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class getscore : MonoBehaviour
{
    public int score = 0;

    public Text ScoreText;

    void Start()
    {
        //get the score
        score = PlayerPrefs.GetInt("score");
        //write ui
        ScoreText.text = "Coins: " + score.ToString();
    }

}
