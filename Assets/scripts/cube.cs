using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class cube : MonoBehaviour
{
    public int score = 0;

    public Text ScoreText;

    void Start()
    {
        //get the score
        score = PlayerPrefs.GetInt("score");
    }

    private void OnCollisionEnter(Collision collision)
    {
        //hit barrier
        if (collision.gameObject.CompareTag("Barrier"))
        {
            //remove score
            score -= 2;

            //write ui
            ScoreText.text = "Coins: " + score.ToString();
        }

        //hit killer
        if (collision.gameObject.CompareTag("Killer"))
        {
            //save score for the next screen
            PlayerPrefs.SetInt("score", score);
            //load finishing screen
            SceneManager.LoadScene("gameover", LoadSceneMode.Single);
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        //hit coin
        if (collision.gameObject.CompareTag("Coin"))
        {
            //add score
            score += 1;
        }

        //level 1 finished
        if (collision.gameObject.CompareTag("Coin2"))
        {
            //add score
            score += 10;
            //save score for the next screen
            PlayerPrefs.SetInt("score", score);
            //load level 2
            SceneManager.LoadScene("lvl2", LoadSceneMode.Single);
        }

        //level 2 finished
        if (collision.gameObject.CompareTag("Coin3"))
        {
            //add score
            score += 10;
            //save score for the next screen
            PlayerPrefs.SetInt("score", score);
            //load finishing screen
            SceneManager.LoadScene("lvl3", LoadSceneMode.Single);
        }

        //level 3 finished
        if (collision.gameObject.CompareTag("Coin4"))
        {
            //add score
            score += 50;
            //save score for the next screen
            PlayerPrefs.SetInt("score", score);
            //load finishing screen
            SceneManager.LoadScene("gameover", LoadSceneMode.Single);
        }

        //write ui
        ScoreText.text = "Coins: " + score.ToString();
        //kill coin
        Destroy(collision.gameObject);
    }
    public void RestartGame()
    {
        //set score back to 0
        PlayerPrefs.SetInt("score", 0);
        //load lvl1
        SceneManager.LoadScene("lvl1", LoadSceneMode.Single);
    }
    public void HowToPlay()
    {
        //load how to play
        SceneManager.LoadScene("howtoplay", LoadSceneMode.Single);
    }
    public void Menu()
    {
        //load menu
        SceneManager.LoadScene("menu", LoadSceneMode.Single);
    }
}
